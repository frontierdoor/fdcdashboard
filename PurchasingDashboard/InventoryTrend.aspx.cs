﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PurchasingDashboard
{
    public partial class InventoryTrend : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "Op Metric - Inventory Trend";
            double subScale = 0.5;

            InventoryByDiv p = new InventoryByDiv();
            if (!Page.IsPostBack)
            {
                foreach (ListItem l in p.PeriodList())
                    PeriodSelection.Items.Add(l);
                PeriodSelection.SelectedIndex = 4; // default to Last 30days
            }

            PeriodSelection_SelectedIndexChanged(sender, e);

        }

        protected void PeriodSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            ColSngl.Controls.Clear();
            InventoryByDiv p = new InventoryByDiv();
            p.SetPeriod(int.Parse(PeriodSelection.SelectedValue));
            ColSngl.Controls.Add(p.chart.LineControl());
        }
    }
}