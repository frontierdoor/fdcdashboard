﻿<%@ Page Title="Purchasing Metric - Acknowledged PO's" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Purchasing.aspx.cs" Inherits="PurchasingDashboard.Purchasing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="jumbotron-db">
        <div class="colA-h">
            <h2>Purchasing Dashboard</h2>
            <h3>Acknowledged Purchase Orders</h3>
        </div>
        <div class="colB-h">
            <asp:PlaceHolder ID="OverAll" runat="server"></asp:PlaceHolder>
        </div>
    </div>
<br />

    <div class="col-1">    
        <asp:PlaceHolder ID="Col1" runat="server"></asp:PlaceHolder>
    </div>

    <div class="col-2">    
        <asp:PlaceHolder ID="Col2" runat="server"></asp:PlaceHolder>
    </div>

    <div class="col-3">
        <asp:PlaceHolder ID="Col3" runat="server"></asp:PlaceHolder>
    </div>

</asp:Content>
