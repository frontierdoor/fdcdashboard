﻿<%@ Page Title="PM Metric - Schedule Change" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ScheduleChanges.aspx.cs" Inherits="PurchasingDashboard.ScheduleChanges" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    
    <div class="jumbotron-db">
        <div class="col-h">
            <h2>Project Manager Dashboard</h2>
            <h3>ScheduleChanges</h3>
        </div>

    </div>
<br />

    <div class="col-1">    
        <asp:PlaceHolder ID="Col1" runat="server"></asp:PlaceHolder>
    </div>

    <div class="col-2">    
        <asp:PlaceHolder ID="Col2" runat="server"></asp:PlaceHolder>
    </div>

    <div class="col-3">
        <asp:PlaceHolder ID="Col3" runat="server"></asp:PlaceHolder>
    </div>

</asp:Content>
