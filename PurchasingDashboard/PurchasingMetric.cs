﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PurchasingDashboard
{
    public class PurchasingMetric
    {
        static private double Red = 0;
        static private double Yellow = 0.65;
        static private double Green = 0.75;

        public enum PurchasingMetrics {
            NonAcknowledgedPOs = 1,
            LatePOs = 2
        }
        public enum PurchasingRoles
        {
            Overall = 0,
            Buyer = 1,
            SeniorPM = 2,
            PM = 3
        }

        public int Metric
        {
            get;set;
        }
        public int Role
        {
            get; set;
        }
        public Dictionary<string, gauge> Gauges;


        //  Class Functions
        public PurchasingMetric(PurchasingMetric.PurchasingMetrics metric)
        {
            Metric = (int) metric;
            Gauges = new Dictionary<string, gauge>();
        }

        //  Public Functions
        public void SetRole(PurchasingMetric.PurchasingRoles role)
        {
            Role = (int)role;
            _updateData();
        }


        //  Private Functions
        private void _updateData()
        {
            switch (Role)
            {
                case 0:
                    _collectOverAll();
                    break;
                case 1:
                    _collectBuyer();
                    break;
                case 2:
                    _collectSeniorPM();
                    break;

            }

        }

        private void _collectBuyer()
        {
            Gauges.Clear();

            float level = 0;

            string strSQL = "SELECT mgr.LastName As MGRLastName, mgr.FirstName As MGRFirstName, result.FirstName AS BUYFirstName, result.LastName AS BUYLastName, sum(result.Yes) as YES, SUM(result.No) as NO FROM People mgr JOIN PeopleRoles as r on mgr.ID = r.PeopleID JOIN PeopleManager as b on mgr.ID = b.ManagerID Join POAckByBuyerID as result on b.PeopleID = result.ID WHERE r.RoleID = 2 GROUP BY mgr.LastName, mgr.FirstName, result.FirstName, result.LastName HAVING    sum(result.Yes) is not null";
            string _strConn = ConfigurationManager.ConnectionStrings["Dashboard"].ConnectionString;
            try
            {
                using (var sc = new SqlConnection(_strConn))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = strSQL;
                    SqlDataReader r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        string Name = r["BUYFirstName"].ToString().Trim() + " " + r["BUYLastName"].ToString().Trim();
                        string Title = r["MGRFirstName"].ToString().Substring(0, 1) + r["MGRLastName"].ToString().Trim();
                        level = (float)(Convert.ToDouble(r["YES"]) / (Convert.ToDouble(r["YES"]) + Convert.ToDouble(r["NO"])));

                        gauge g = new gauge(Name, Red, Yellow, Green, level, 1);
                        Gauges.Add(Name, g);
                    }
                }
            }
            catch (Exception e)
            {
                bool _errorstate = true;
            }

        }

        private void _collectSeniorPM()
        {
            Gauges.Clear();

            float level = 0;

            string strSQL = "SELECT mgr.LastName As LastName, mgr.FirstName As FirstName, sum(result.Yes) as YES, SUM(result.No) as NO FROM People mgr JOIN PeopleRoles as r on mgr.ID = r.PeopleID JOIN PeopleManager as b on mgr.ID = b.ManagerID Join POAckByBuyerID as result on b.PeopleID = result.ID WHERE r.RoleID = 2 GROUP BY mgr.LastName, mgr.FirstName HAVING    sum(result.Yes) is not null";
            string _strConn = ConfigurationManager.ConnectionStrings["Dashboard"].ConnectionString;
            try
            {
                using (var sc = new SqlConnection(_strConn))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = strSQL;
                    SqlDataReader r = cmd.ExecuteReader();
                    while(r.Read())
                    {
                        string Name = r["FirstName"].ToString().Trim() + " " + r["LastName"].ToString().Trim();
                        string Title = r["FirstName"].ToString().Substring(0, 1) + r["LastName"].ToString().Trim();
                        level = (float)(Convert.ToDouble(r["YES"]) / (Convert.ToDouble(r["YES"]) + Convert.ToDouble(r["NO"])));

                        gauge g = new gauge(Name, Red, Yellow, Green, level, 1);
                        Gauges.Add(Name, g);
                    }
                }
            }
            catch (Exception e)
            {
                bool _errorstate = true;
            }

        }

        private void _collectOverAll()
        {
            Gauges.Clear();

            float level = 0;

            string strSQL = "SELECT TOP 1 CAST(Data AS FLOAT) FROM MetricHistory WHERE MetricID =" + Metric + " ORDER BY RecordDate DESC";
            string _strConn = ConfigurationManager.ConnectionStrings["Dashboard"].ConnectionString;
            try
            {
                using (var sc = new SqlConnection(_strConn))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = strSQL;
                    level = (float)((double)cmd.ExecuteScalar());
                    sc.Close();
                }
            }
            catch (Exception e)
            {
                bool _errorstate = true;
            }

            gauge g = new gauge("Overall", Red, Yellow, Green, level, 1);
            Gauges.Add("OverAll", g);

        }

    }
}