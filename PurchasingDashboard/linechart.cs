﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;


namespace PurchasingDashboard
{
    public struct LineChartData
    {
        public string Label;
        public double Data;
        public string Color;
    }

    public class linechart
    {
        public string Name { get; set; }
        public string ID { get; set; }

        public double Level { get; set; }
        public double Scale { get; set; }
        public List<string> Labels;
        public Dictionary<string, string> Colors;
        public Dictionary<string, List<double>> DataSets;


        //  Class Functions
        public linechart(string _name)
        {
            Name = _name;
            Labels = new List<string>();
            Colors = new Dictionary<string, string>();
            DataSets = new Dictionary<string, List<double>>();
            _preload();
        }

        //  Public Functions
        public HtmlGenericControl LineControl(double scale = 0.0)
        {
            if(scale != 0 ) Scale = scale;

            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "linechart");
            mydiv.Attributes.Add("id", Name);
            mydiv.Controls.Add(linechartHeader());
            mydiv.Controls.Add(linechartBody());

            return mydiv;
        }

        //  Private Functions
        private void _preload()
        {
            Level = .50;
            Scale = 1;
        }

        private HtmlGenericControl linechartBody()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "linechart-data");
            mydiv.InnerHtml = "<canvas id=\"" + ID + "\"></canvas>";
            mydiv.InnerHtml += linechartScript();
            return mydiv;
        }

        private HtmlGenericControl linechartHeader()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "linechart-header");
            mydiv.InnerHtml = Name;
            return mydiv;
        }

        private string dsLabels()
        {
            string l = "labels: [";
            int c = 0;

            foreach (string entry in Labels)
            {
                c++;
                l += "\"" + entry + "\"";
                if (c < Labels.Count) l += ",";
            }
            l += "]";

            return l;
        }
        
        private string dsData(List<double> li)
        {
            string l = "\t\t\tdata: [";
            int c = 0;

            foreach (double d in li)
            {
                c++;
                l += d.ToString() ;
                if (c < li.Count) l += ",";
            }
            l += "]";

            return l;
        }

        private string linechartScript()
        {
            string jsScript = "";

            jsScript += "<script type=\"text/javascript\">\n";

            jsScript += "var ctx = document.getElementById(\"" + ID + "\").getContext('2d');\n";
            jsScript += "var myChart = new Chart(ctx,\n\t{type: 'line',\n\tdata: {\n";
            jsScript += dsLabels() + ",\n";
            jsScript += "\tdatasets: [\n";
            int l = 0;
            foreach(KeyValuePair<string, List<double> > entry in DataSets)
            {
                l++;
                jsScript += "\n\t\t{label: \"" + entry.Key + "\", \n";
                jsScript += "\t\tborderColor: \"" + Colors[entry.Key] + "\", fill: false, ";
                jsScript += "\t\tlineTension:0.1, ";
                jsScript += dsData(entry.Value);
                jsScript += "}";
                if (l < DataSets.Count)
                    jsScript += ",\n";
            }

            jsScript += "]}});\n</script>";

            return jsScript;

        }
             
    }

}


 