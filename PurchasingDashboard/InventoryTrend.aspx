﻿<%@ Page Title="PM Metric - Schedule Change" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InventoryTrend.aspx.cs" Inherits="PurchasingDashboard.InventoryTrend" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    
    <div class="jumbotron-db">
        <div class="col-h">
            <h2>Operations Dashboard</h2>
            <h3>InventoryTrend</h3>
            <br />
            <asp:DropDownList ID="PeriodSelection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="PeriodSelection_SelectedIndexChanged">
        </asp:DropDownList>
   <br />
        <asp:PlaceHolder ID="ColSngl" runat="server"></asp:PlaceHolder>

</asp:Content>
