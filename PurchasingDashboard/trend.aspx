﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="trend.aspx.cs" Inherits="PurchasingDashboard.trend" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <div class="jumbotron-db">
        <h2>Dashboards</h2>
    </div>
<br />
    <canvas id="InvByDiv" ></canvas>
 <script type="text/javascript">
var ctx = document.getElementById("InvByDiv").getContext('2d');
var myChart = new Chart(ctx,
	{type: 'line',
	data: {
labels: ["1/10/2021","1/11/2021","1/12/2021","1/13/2021","1/14/2021","1/15/2021","1/16/2021","1/18/2021","1/19/2021","1/20/2021","1/21/2021","1/22/2021","1/25/2021","1/26/2021","1/27/2021","1/28/2021","1/29/2021","2/1/2021","2/2/2021","2/3/2021","2/4/2021","2/5/2021","2/6/2021","2/7/2021","2/8/2021"],
	datasets: [

		{label: "DOORS", 
		bordercolor: "#800000", 		lineTension:0.1, 			data: [2341958,2398557,2493873,2520011,2573781,2573781,2629275,2560643,2711311,2695802,2744419,2548280,2556288,2425596,2384632,2349682,2330421,2335195,2386093,2483219,2545926,2514752,2514752,2514752,2473245]},

		{label: "CABINETS", 
		bordercolor: "#808080", 		lineTension:0.1, 			data: [1488484,1522066,1503377,1492313,1503394,1503394,1485566,1504123,1495684,1415172,1461022,1455416,1488168,1501883,1515673,1394209,1438533,1430402,1485456,1505632,1421004,1427992,1427992,1427992,1443370]}]}});
   
    </script>
</asp:Content>
