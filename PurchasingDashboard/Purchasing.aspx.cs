﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PurchasingDashboard
{
    public partial class Purchasing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "Purchasing Metric - Acknowledged PO's";
            int Columns = 3;
            double subScale = 0.5;

            PurchasingMetric p = new PurchasingMetric(PurchasingMetric.PurchasingMetrics.NonAcknowledgedPOs);
            p.SetRole(PurchasingMetric.PurchasingRoles.Overall);

            OverAll.Controls.Add(p.Gauges["OverAll"].GaugeControl());

            p.SetRole(PurchasingMetric.PurchasingRoles.SeniorPM);
            //p.SetRole(PurchasingMetric.PurchasingRoles.Buyer);

            int count = 0;
            foreach(KeyValuePair<string, gauge> entry in p.Gauges)
            {
                int c = count++ % Columns;
                PlaceHolder ph = new PlaceHolder();
                switch (c)
                {
                    case 0:
                        ph = Col1;
                        break;
                    case 1:
                        ph = Col2;
                        break;
                    case 2:
                        ph = Col3;
                        break;
                }
                
                ph.Controls.Add(entry.Value.GaugeControl(subScale));
            }

        }
    }
}