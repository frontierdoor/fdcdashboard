﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;

namespace PurchasingDashboard
{
    public class gauge
    {
        public string Name { get; set; }
        public double RedStart { get; set; }
        public double YellowStart { get; set; }
        public double GreenStart { get; set; }
        public double Level { get; set; }
        public double Scale { get; set; }
        public double redDegree
        {
            get { return 180  * RedStart; }
        }
        public double yellowDegree
        {
            get { return 180 * YellowStart; }
        }
        public double greenDegree
        {
            get { return 180 * GreenStart; }
        }
        public double levelDegree
        {
            get { return 180 * Level; }
        }

        private int redZIndex;
        private int yellowZIndex;
        private int greenZIndex;

        //  Class Functions
        public gauge(string _name)
        {
            Name = _name;
            _preload();
        }

        public gauge(string name, double Red, double Yellow, double Green, double level, double scale)
        {
            Name = name;
            _preload();
            RedStart = Red;
            YellowStart = Yellow;
            GreenStart = Green;
            Level = level;
            Scale = scale;
        }

        //  Public Functions
        public HtmlGenericControl GaugeControl(double scale = 0.0)
        {
            if(scale != 0 ) Scale = scale;
            _sortColors();

            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge");
            mydiv.Attributes.Add("id", Name);
            mydiv.Controls.Add(gaugeHeader());
            mydiv.Controls.Add(gaugeBody());

            return mydiv;
        }

        //  Private Functions
        private void _preload()
        {
            RedStart = 0;
            YellowStart = 0.333;
            GreenStart = 0.667;
            Level = .50;
            Scale = 1;
        }

        private void _sortColors()
        {
            redZIndex = 10;
            yellowZIndex = 11;
            greenZIndex = 12;

            if (RedStart > YellowStart)
            {
                redZIndex++;
                yellowZIndex--;
            }
            if (RedStart > GreenStart)
            {
                redZIndex++;
                greenZIndex--;
            }
            if (YellowStart > GreenStart)
            {
                yellowZIndex++;
                greenZIndex--;
            }
        }

        public HtmlGenericControl gaugeBody()
        {
            _sortColors();

            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge-body");
            mydiv.Controls.Add(gaugeCenter());
            mydiv.Controls.Add(gaugeRed());
            mydiv.Controls.Add(gaugeYellow());
            mydiv.Controls.Add(gaugeGreen());
            mydiv.Controls.Add(gaugeNeedle());
            mydiv.Controls.Add(gaugeData());
            mydiv.Style.Add("transform", "scale(" + Scale.ToString() + ")");
            return mydiv;
        }

        private HtmlGenericControl gaugeCenter()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge-center");
            return mydiv;
        }

        private HtmlGenericControl gaugeRed()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge-red");
            mydiv.Style.Add("transform", "rotate(" + redDegree.ToString() + "deg)");
            mydiv.Style.Add("z-index", redZIndex.ToString());
            return mydiv;
        }

        private HtmlGenericControl gaugeYellow()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge-yellow");
            mydiv.Style.Add("transform", "rotate(" + yellowDegree.ToString() + "deg)");
            mydiv.Style.Add("z-index", yellowZIndex.ToString());
            return mydiv;
        }

        private HtmlGenericControl gaugeGreen()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge-green");
            mydiv.Style.Add("transform", "rotate(" + greenDegree.ToString() + "deg)");
            mydiv.Style.Add("z-index", greenZIndex.ToString());
            return mydiv;
        }

        private HtmlGenericControl gaugeNeedle()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge-needle");
            mydiv.Style.Add("transform", "rotate(" + levelDegree.ToString() + "deg)");
            return mydiv;
        }

        private HtmlGenericControl gaugeData()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge-data");
            mydiv.InnerText = Level.ToString("P");
            return mydiv;
        }

        private HtmlGenericControl gaugeHeader()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "gauge-header");
            mydiv.InnerText = Name;
            return mydiv;
        }
        /* 
         *     <div class="gauge" id="Overall">
        <div class="gauge-center"></div>
        <div class="gauge-red"></div>
        <div class="gauge-yellow"></div>
        <div class="gauge-green"></div>
        <div class="gauge-needle" style="transform: rotate(45deg);"></div>
        <div class="gauge-data">
            <div id="percent">30%</div>
        </div>
    </div>

    */
    }
}

 