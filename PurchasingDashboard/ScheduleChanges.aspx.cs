﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PurchasingDashboard
{
    public partial class ScheduleChanges : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "PM Metric - Schedule Change";
            int Columns = 3;
            double subScale = 0.5;

            //HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            //mydiv.InnerHtml = "&nbsp;";
            //OverAll.Controls.Add(mydiv);

            ScheduleChangeMetric p = new ScheduleChangeMetric();
            p.SetRole(ScheduleChangeMetric.ScheduleChangeRoles.PM);

            int count = 0;
            foreach(KeyValuePair<string, piechart> entry in p.Charts)
            {
                int c = count++ % Columns;

                PlaceHolder ph = new PlaceHolder();
                switch (c)
                {
                    case 0:
                        ph = Col1;
                        break;
                    case 1:
                        ph = Col2;
                        break;
                    case 2:
                        ph = Col3;
                        break;
                }
                
                ph.Controls.Add(entry.Value.PieControl(subScale));
            }

        }
    }
}