﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;


namespace PurchasingDashboard
{
    public struct PieData
    {
        public double Data;
        public string Color;
    }

    public class piechart
    {
        public string Name { get; set; }
        public string ID { get; set; }

        public double Level { get; set; }
        public double Scale { get; set; }
        public Dictionary<string, PieData> DataSet;


        //  Class Functions
        public piechart(string _name)
        {
            Name = _name;
            DataSet = new Dictionary<string, PieData>();
            _preload();
        }

        public piechart(string name, Dictionary<string, PieData> dataset)
        {
            Name = name;
            ID = Regex.Replace(name, @"\s+", "");
            DataSet = new Dictionary<string, PieData>(dataset);
            _preload();
        }

        //  Public Functions
        public HtmlGenericControl PieControl(double scale = 0.0)
        {
            if(scale != 0 ) Scale = scale;

            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "pie");
            mydiv.Attributes.Add("id", Name);
            mydiv.Controls.Add(pieHeader());
            mydiv.Controls.Add(pieBody());

            return mydiv;
        }

        //  Private Functions
        private void _preload()
        {
            Level = .50;
            Scale = 1;
        }

        private HtmlGenericControl pieBody()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "pie-data");
            mydiv.InnerHtml = "<canvas id=\"" + ID + "\"></canvas>";
            mydiv.InnerHtml += pieScript();
            return mydiv;
        }

        private HtmlGenericControl pieHeader()
        {
            HtmlGenericControl mydiv = new HtmlGenericControl("DIV");
            mydiv.Attributes.Add("class", "pie-header");
            mydiv.InnerHtml = Name;
            return mydiv;
        }

        private string dsLabels()
        {
            string l = "labels: [";
            int c = 0;

            foreach (KeyValuePair<string, PieData> entry in DataSet)
            {
                c++;
                l += "\"" + entry.Key + "\"";
                if (c < DataSet.Count) l += ",";
            }
            l += "]";

            return l;
        }
        private string dsColors()
        {
            string l = "backgroundColor: [";
            int c = 0;

            foreach (KeyValuePair<string, PieData> entry in DataSet)
            {
                c++;
                l += "\"" + entry.Value.Color + "\"";
                if (c < DataSet.Count) l += ",";
            }
            l += "]";

            return l;
        }
        private string dsData()
        {
            string l = "data: [";
            int c = 0;

            foreach (KeyValuePair<string, PieData> entry in DataSet)
            {
                c++;
                l += entry.Value.Data ;
                if (c < DataSet.Count) l += ",";
            }
            l += "]";

            return l;
        }

        private string pieScript()
        {
            string jsScript = "";

            jsScript += "<script type=\"text/javascript\">";

            jsScript += "var ctx = document.getElementById(\"" + ID + "\").getContext('2d');";
            jsScript += "var myChart = new Chart(ctx, { type: 'pie',  data: { ";
            jsScript += dsLabels() + ",";
            jsScript += "datasets: [{ ";
            jsScript += dsColors() + ",";
            jsScript += dsData();
            jsScript += "}]}});</script>";

            return jsScript;

        }
             
    }

}


 