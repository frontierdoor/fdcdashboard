﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PurchasingDashboard
{
    public class ScheduleChangeMetric
    {
        static private string RUSH = "#FF0000";
        static private string LATE = "#FFFF00";
        static private string GOOD = "#90EE90";
        static private int DAYS = 28;

        public enum ScheduleChangeClass {
            Rush = 1,
            Late = 2,
            Good = 3
        }
        public enum ScheduleChangeRoles
        {
            Overall = 0,
            Buyer = 1,
            SeniorPM = 2,
            PM = 3
        }

        public int Role
        {
            get; set;
        }
        public Dictionary<string, piechart> Charts;


        //  Class Functions
        public ScheduleChangeMetric()
        {
            Charts = new Dictionary<string, piechart>();
        }

        //  Public Functions
        public void SetRole(ScheduleChangeRoles role)
        {
            Role = (int)role;
            _updateData();
        }


        //  Private Functions
        private void _updateData()
        {
            Charts.Clear();
            switch (Role)
            {
                case 0:
                    _collectOverAll();
                    break;
                case 1:
                    // _collectPM();
                    break;
                case 2:
                    _collectSeniorPM();
                    break;
                case 3:
                    _collectPM();
                    break;

            }

        }

        private void _collectSeniorPM()
        {

            int LastXDays = DAYS;

            string strSQL = "SELECT mgr.LastName As LastName, mgr.FirstName As FirstName, sum(result.[1]) as RUSH, SUM(result.[2]) as LATE, SUM(result.[3]) as GOOD FROM People mgr JOIN PeopleRoles as r on mgr.ID = r.PeopleID JOIN PeopleManager as b on mgr.ID = b.ManagerID left outer Join (SELECT PM, [1], [2],[3] from (SELECT SOID, People.ID as PM, (CASE WHEN DaysToShipDate >= 0 AND DaysToShipDate < 7 THEN 1 WHEN DaysToShipDate >= 7 AND DaysToShipDate < 14 THEN 2 ELSE 3 END) As Age FROM [SOExpctDate] JOIN SOData ON SOData.SOID = TranID JOIN People ON SOData.PM = People.SaleRepID WHERE datediff(d, CreatedDate, getdate()) <= " + LastXDays + ") d pivot (count(SOID) for Age in ( [1], [2],[3]) ) piv) as result on b.PeopleID = result.PM WHERE r.RoleID = 2 GROUP BY mgr.LastName, mgr.FirstName";
            string _strConn = ConfigurationManager.ConnectionStrings["Dashboard"].ConnectionString;
            try
            {
                using (var sc = new SqlConnection(_strConn))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = strSQL;
                    SqlDataReader r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        string Name = r["FirstName"].ToString().Trim() + " " + r["LastName"].ToString().Trim();
                        if (Name == "CABINET BUYERS" || Name == "TRENT HAXBY") continue;
                        PieData rush, late, good;

                        rush.Data = (double)(Convert.ToDouble(r["RUSH"]));
                        rush.Color = RUSH;
                        late.Data = (double)(Convert.ToDouble(r["LATE"]));
                        late.Color = LATE;
                        good.Data = (double)(Convert.ToDouble(r["GOOD"]));
                        good.Color = GOOD;

                        Dictionary<string, PieData> d = new Dictionary<string, PieData>();
                        d.Add("RUSH", rush);
                        d.Add("LATE", late);
                        d.Add("GOOD", good);

                        piechart p = new piechart(Name, d);
                        Charts.Add(Name, p);
                    }
                }
            }
            catch (Exception e)
            {
                string message = e.Message;
                bool _errorstate = true;
            }
        }

        private void _collectPM()
        {

            int LastXDays = DAYS;

            string strSQL = "SELECT mgr.LastName As LastName, mgr.FirstName As FirstName, sum(result.[1]) as RUSH, SUM(result.[2]) as LATE, SUM(result.[3]) as GOOD FROM People mgr JOIN PeopleRoles as r on mgr.ID = r.PeopleID left outer Join (SELECT PM, [1], [2],[3] from (SELECT SOID, People.ID as PM, (CASE WHEN DaysToShipDate >= 0 AND DaysToShipDate < 7 THEN 1 WHEN DaysToShipDate >= 7 AND DaysToShipDate < 14 THEN 2 ELSE 3 END) As Age FROM [SOExpctDate] JOIN SOData ON SOData.SOID = TranID JOIN People ON SOData.PM = People.SaleRepID WHERE SoData.ShipVia <> 'PCCS' AND datediff(d, CreatedDate, getdate()) <= " + LastXDays + ") d pivot (count(SOID) for Age in ( [1], [2],[3]) ) piv) as result on r.PeopleID = result.PM WHERE r.RoleID = 3 GROUP BY mgr.LastName, mgr.FirstName";
            string _strConn = ConfigurationManager.ConnectionStrings["Dashboard"].ConnectionString;
            try
            {
                using (var sc = new SqlConnection(_strConn))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = strSQL;
                    SqlDataReader r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        string Name = r["FirstName"].ToString().Trim() + " " + r["LastName"].ToString().Trim();
                        PieData rush, late, good;
                        try
                        {

                            rush.Data = (double)(Convert.ToDouble(r["RUSH"]));
                            rush.Color = RUSH;
                            late.Data = (double)(Convert.ToDouble(r["LATE"]));
                            late.Color = LATE;
                            good.Data = (double)(Convert.ToDouble(r["GOOD"]));
                            good.Color = GOOD;


                            Dictionary<string, PieData> d = new Dictionary<string, PieData>();
                            d.Add("RUSH", rush);
                            d.Add("LATE", late);
                            d.Add("GOOD", good);

                            piechart p = new piechart(Name, d);
                            Charts.Add(Name, p);
                        }
                        catch (InvalidCastException e)
                        {
                            // DBNUll should just be ignored.
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string message = e.Message;
                bool _errorstate = true;
            }
        }


        private void _collectOverAll()
        {

            int LastXDays = DAYS;

            string strSQL = "SELECT sum(result.[1]) as RUSH, SUM(result.[2]) as LATE, SUM(result.[3]) as GOOD FROM (SELECT[1], [2], [3] from( SELECT SOID, (CASE WHEN DaysToShipDate >= 0 AND DaysToShipDate < 7 THEN 1 WHEN DaysToShipDate >= 7 AND DaysToShipDate < 14 THEN 2 ELSE 3 END) As Age FROM[SOExpctDate] JOIN SOData ON SOData.SOID = TranID WHERE datediff(d, CreatedDate, getdate()) <= " + LastXDays + ") d pivot (count(SOID) for Age in ( [1], [2],[3]) ) piv) as result";
            string _strConn = ConfigurationManager.ConnectionStrings["Dashboard"].ConnectionString;
            try
            {
                using (var sc = new SqlConnection(_strConn))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = strSQL;
                    SqlDataReader r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        string Name = "OVERALL";
                        PieData rush, late, good;

                        rush.Data = (double)(Convert.ToDouble(r["RUSH"]));
                        rush.Color = RUSH;
                        late.Data = (double)(Convert.ToDouble(r["LATE"]));
                        late.Color = LATE;
                        good.Data = (double)(Convert.ToDouble(r["GOOD"]));
                        good.Color = GOOD;

                        Dictionary<string, PieData> d = new Dictionary<string, PieData>();
                        d.Add("RUSH", rush);
                        d.Add("LATE", late);
                        d.Add("GOOD", good);

                        piechart p = new piechart(Name, d);
                        Charts.Add(Name, p);
                    }
                }
            }
            catch (Exception e)
            {
                string message = e.Message;
                bool _errorstate = true;
            }

        }

    }



}