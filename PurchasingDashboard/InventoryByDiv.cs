﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PurchasingDashboard
{
    public class InventoryByDiv
    {
        static private string DOOR = "#800000";
        static private string CAB = "#808080";

        public enum InventoryPeriods
        {
            ThisMonth = 0,
            LastMonth = 1,
            ThisQuarter = 2,
            ThisYear = 3,
            Last30Days = 4,
            LastSixMonths = 5,
            LastTwelveMonths = 6
        }

        public linechart chart;
        private DateTime _start;
        private DateTime  _end;
        private List<ListItem> _periods;

        //  Class Functions
        public InventoryByDiv()
        {
            chart = new linechart("InventoryTrends");
            chart.ID = "InvByDiv";
            _start = new DateTime();
            _end = new DateTime();
            _periods = new List<ListItem>();
            _buildperiods();
        }

        //  Public Functions
        public void SetPeriod(int selectedperiod)
        {
            _setdates(selectedperiod);

            _collectdata();
        }

        public List<ListItem> PeriodList()
        {
            return _periods;
        }


        //  Private Functions
        private void _setdates(int selectedperiod)
        {
            DateTime now = DateTime.Now;
            DateTime h, s, e;

            switch (selectedperiod)
            {
                // ThisMonth = 0,
                case 0:
                    s = new DateTime(now.Year, now.Month, 1);
                    e = now;
                    break;
                // LastMonth = 1,
                case 1:
                    h = new DateTime(now.Year, now.Month, 1);
                    s = h.AddMonths(-1);
                    e = s.AddMonths(1).AddDays(-1);
                    break;
                // ThisQuarter = 2,
                //case 2:
                //    _start = new DateTime(now.Year, now.Month, 1);
                //    _end = _start.AddMonths(1).AddDays(-1); ;
                //    break;
                // ThisYear = 3,
                case 3:
                    s = new DateTime(now.Year, 1, 1);
                    e = now;
                    break;
                // Last30Days = 4,
                case 4:
                    s = now.AddDays(-30);
                    e = now;
                    break;
                // LastSixMonths = 5,
                case 5:
                    s = now.AddMonths(-6);
                    e = now;
                    break;
                // LastTwelveMonths = 6
                case 6:
                    s = now.AddMonths(-12);
                    e = now;
                    break;
                default:
                    s = now.AddDays(-30);
                    e = now;
                    break;
            }
            _start = s;
            _end = e;

        }
        private void _buildperiods()
        {
            _periods.Clear();

            // ThisMonth = 0,
            _periods.Add(new ListItem("This Month", "0"));
            // LastMonth = 1,
            _periods.Add(new ListItem("Last Month", "1"));
            // ThisQuarter = 2,
            _periods.Add(new ListItem("This Quarter", "2"));
            // ThisYear = 3,
            _periods.Add(new ListItem("This Year", "3"));
            // Last30Days = 4,
            _periods.Add(new ListItem("Last 30 days", "4"));
            // LastSixMonths = 5,
            _periods.Add(new ListItem("Last 6 months", "5"));
            // LastTwelveMonths = 6
            _periods.Add(new ListItem("Last 12 months", "6"));

        }

        private void _collectdata()
        {
            chart.Colors.Clear();
            chart.Colors.Add("DOORS", DOOR);
            chart.Colors.Add("CABINETS", CAB);
            chart.DataSets.Add("DOORS", new List<double>());
            chart.DataSets.Add("CABINETS", new List<double>());

            string strSQL = "SELECT  Division, Sum(Amount) as Amount, DateOf FROM InvByLocation JOIN LocByDivision on InvByLocation.Location = LocByDivision.Location  WHERE DateOf between '" + _start.ToShortDateString() + "' and '" + _end.ToShortDateString() + "' GROUP BY Division, DateOf ORDER BY DateOf";
            string _strConn = ConfigurationManager.ConnectionStrings["Dashboard"].ConnectionString;
            try
            {
                using (var sc = new SqlConnection(_strConn))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = strSQL;
                    SqlDataReader r = cmd.ExecuteReader();

                    while (r.Read())
                    {
                        DateTime h = Convert.ToDateTime(r["DateOf"]);
                        if (! chart.Labels.Contains(h.ToShortDateString()))
                            chart.Labels.Add(h.ToShortDateString());
                        chart.DataSets[Convert.ToString(r["Division"]).Trim()].Add((double)(Convert.ToDouble(r["Amount"])));
                    }
                }
            }
            catch (Exception e)
            {
                string message = e.Message;
                bool _errorstate = true;
            }

        }

    }



}